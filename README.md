# **DB Parkplatz App**
---

## Überblick
Die DB-Parkplatzapp soll bei der Suche nach Parkplätzen in der Nähe von Bahnhöfen helfen. Das ortsbasierte Suchen soll sowohl durch die Eingabe eines Ortes, als auch durch Positionsbestimmung mittels GPS möglich sein. Angezeigt werden Entfernung sowie Auslastung und Preise (falls verfügbar). Die zugrunde liegenden Informationen werden von der Deutsche Bahn zur Verfügung gestellt.


## Merkmale
* Die Daten stammen aus der Open Daten Quelle: [ DB Parkplatz API ](http://data.deutschebahn.com/dataset/api-parkplatz) 
* Daten werden regelmäßig von der App automatisch aktualisiert.
* Verfügbare Parkplätze werden auf der Karte angezeigt.
* Die Anzahl der Freien Stellplätze wird farblich dargestellt.
* Parkplätze werden in einer Liste, sortiert nach Entfernung zum aktuellen Standort, angezeigt.

  

## Installation
Zum Entwickeln sollte Android Studio verwendet werden.
Dazu werden folgende Komponenten benötigt:
 
* Android APK-25
* Gradle 2.14.1 oder höher
* Ein Emulator oder Android Gerät mit SDK-16 oder höher
  

## Usage
Um die Liste der Parkplätze, sortiert nach Entfernung zum aktuellen Standort, muss auf _in meiner Nähe finden_ geklickt werden. Mit einem Klick auf die Liste wird die Karte aufgerufen und zum angeklickten Parkplatz gezoomt.  

Um an einem Ort verfügbare Parkplätze anzuzeigen, muss im Hauptmenü auf _an bestimmten Ort finden_ geklickt werden. Dann wird die Karte angezeigt und es kann nach einem Ort gesucht werden.  

Parkplätze werden stets auf der Karte angezeigt.
Mit einem Klick auf einen Parkplatz können mehr Informationen abgerufen werden.

  
---
Mehr Informationen finden Sie in unseren [ Konzept ](https://bitbucket.org/tms_ws16_05/konzept).