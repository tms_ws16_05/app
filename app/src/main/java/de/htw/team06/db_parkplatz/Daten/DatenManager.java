package de.htw.team06.db_parkplatz.Daten;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

public final class DatenManager {

    //Lint-Fehler lässt sich nicht umgehen, Code für Singleton wird selbst in Developer Dokumentation so verwendet
    //siehe http://stackoverflow.com/questions/37709918/warning-do-not-place-android-context-classes-in-static-fields-this-is-a-memory
    private static DatenManager _singleton;

    private static final String RESPONSE_RESULT_KEY = "results";

    private static final String RESPONSE_BELEGUNGEN_KEY = "allocations";
    private static final String RESPONSE_PARKPLATZ_INFO_KEY = "site";
    private static final String RESPONSE_ID_KEY = "id";
    private static final String RESPONSE_BELEGUNG_KEY = "allocation";
    private static final String RESPONSE_KATEGORY_KEY = "category";
    private static final String RESPONSE_KATEGORY_TEXT_KEY = "text";

    private static final String FILE_NAME_PARKPLATZ = "parkplatz.json";
    private static final long TIME_UNTIL_UPDATE_IN_MS = 24 * 60 * 60 * 1000; //Tag * Stunden * Minuten * Sekunden * MilliSekunden
    private static final long MS_ZU_STUNDE_DIVISOR =  1000 * 60 * 60;
    private static final String URL_PARKPLAETZE = "http://opendata.dbbahnpark.info/api/beta/sites";
    private static final String URL_PARKPLAETZE_BELEGUNG = "http://opendata.dbbahnpark.info/api/beta/occupancy";

    private Context _context = null;
    private IParkplatz[] _parkplaetze = null;

    //keine Instanz
    private DatenManager(Context con) {
        _context = con;
    }

    public static void init(Context con) {
        _singleton = new DatenManager(con);
        if (con != null)
            _singleton.checkUpdate();
    }

    public static DatenManager getInstance() {
        return _singleton;
    }

    private void parseParkplaetzeBelegung(String stringToParse) {


        try {
            JSONObject alles = new JSONObject(stringToParse);
            JSONArray alleEintraege = alles.getJSONArray(RESPONSE_BELEGUNGEN_KEY);
            for (int i = 0; i < alleEintraege.length(); i++) {
                JSONObject belegung = alleEintraege.getJSONObject(i);
                int belegungId = belegung.getJSONObject(RESPONSE_PARKPLATZ_INFO_KEY).optInt(RESPONSE_ID_KEY, -1);
                if (belegungId > 0) {
                    int categoryNummer = belegung.getJSONObject(RESPONSE_BELEGUNG_KEY).optInt(RESPONSE_KATEGORY_KEY, Parkplatz.DEFAULT_BELEGUNG_KATEGORIE_NUMMER);
                    String categoryText = belegung.getJSONObject(RESPONSE_BELEGUNG_KEY).optString(RESPONSE_KATEGORY_TEXT_KEY, Parkplatz.DEFAULT_BELEGUNG_KATEGORIE_TEXT);

                    for (IParkplatz p : _parkplaetze) {
                        if (p.getID() == belegungId) {
                            p.setBelegungKategory(categoryNummer, categoryText);
                            break;
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseParkplaetze(String stringToParse) {
        ArrayList<Parkplatz> alleParkplaetze = new ArrayList<>();

        try {
            JSONObject alles = new JSONObject(stringToParse);
            JSONArray alleEintraege = alles.getJSONArray(RESPONSE_RESULT_KEY);
            for (int i = 0; i < alleEintraege.length(); i++) {
                JSONObject parkplatz = alleEintraege.getJSONObject(i);
                alleParkplaetze.add(new Parkplatz(parkplatz));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        _parkplaetze = alleParkplaetze.toArray(new IParkplatz[0]);
    }

    public IParkplatz[] getAlleParkplaetze() {
        try {
            if (_parkplaetze == null)
                parseParkplaetze(readOfflineParkplaetze());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return _parkplaetze;
    }

    private String readOfflineParkplaetze() throws IOException {
        return readOfflineFile();
    }

    private String readOfflineFile() throws IOException {
        String str;
        StringBuilder buf = new StringBuilder();
        InputStream is = _context.getResources().getAssets().open("liste_parkplaetze.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        if (is != null) {
            while ((str = reader.readLine()) != null) {
                buf.append(str).append("\n");
            }
        }
        is.close();

        return buf.toString();
    }

    private void checkUpdate() {

        boolean updateParkplaetze = false;

        File parkplaetze = new File(_context.getFilesDir(), FILE_NAME_PARKPLATZ);
        if (!parkplaetze.exists())
            try {
                parkplaetze.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        else {
            Date lastUpdate = new Date(parkplaetze.lastModified());
            long diff = new Date().getTime() - lastUpdate.getTime();
            if (diff > TIME_UNTIL_UPDATE_IN_MS) {
                Log.d("Update Daten:", " --> Update Nötig! letztes Update vor " + diff / MS_ZU_STUNDE_DIVISOR + " h");
                updateParkplaetze = true;
            } else
                Log.d("Update Daten:", " --> Kein Update Nötig! letztes Update vor " + ((float) diff) / MS_ZU_STUNDE_DIVISOR  + " h");
        }

        datenHerunterladen(updateParkplaetze);
        _parkplaetze = getAlleParkplaetze();

    }

    private void datenHerunterladen(boolean needUpdateParkplaetze) {

        URL parkplaetze, belegung;
        try {
            parkplaetze = new URL(URL_PARKPLAETZE);
            belegung = new URL(URL_PARKPLAETZE_BELEGUNG);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }
        URL[] urlsArray = {needUpdateParkplaetze ? parkplaetze : null, belegung};

        new DownloadFilesTask().execute(urlsArray);
    }

// --Commented out by Inspection START (24.01.17 21:10):
//    private String ladeDatei(String dateiPfad) {
//        StringBuilder stringBuilder = new StringBuilder();
//        FileInputStream fileInStream = null;
//        InputStreamReader inStreamReader = null;
//        BufferedReader bufReader = null;
//
//        try {
//            fileInStream = _context.openFileInput(dateiPfad);
//            inStreamReader = new InputStreamReader(fileInStream);
//            bufReader = new BufferedReader(inStreamReader);
//            String s;
//            while ((s = bufReader.readLine()) != null) {
//                if (s.length() != 0) {
//                    stringBuilder.append('\n');
//                }
//                stringBuilder.append(s);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        } finally {
//            if (fileInStream != null) {
//                try {
//                    fileInStream.close();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            if (inStreamReader != null) {
//                try {
//                    inStreamReader.close();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            if (bufReader != null) {
//                try {
//                    bufReader.close();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        return stringBuilder.toString();
//    }
// --Commented out by Inspection STOP (24.01.17 21:10)

    private void speicherDatei(String text) {

        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        try {
            fos = _context.openFileOutput(DatenManager.FILE_NAME_PARKPLATZ, Context.MODE_PRIVATE);
            osw = new OutputStreamWriter(fos);
            osw.write(text);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (osw != null) {
                try {
                    osw.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }

        }
    }


    private class DownloadFilesTask extends AsyncTask<URL, Integer, String[]> {
        protected String[] doInBackground(URL... urls) {

            String[] result = new String[urls.length];

            for (int i = 0; i < urls.length; i++) {
                if (urls[i] == null)
                    continue;

                InputStream is = null;

                try {
                    URL url = urls[i];

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);

                    conn.connect();

                    is = conn.getInputStream();

                    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                    StringBuilder sb = new StringBuilder();
                    String str;
                    while ((str = bufferReader.readLine()) != null) {
                        sb.append(str);
                    }

                    result[i] = sb.toString();

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            return result;
        }

        protected void onPostExecute(String[] result) {
            if (result[0] != null) {
                speicherDatei(result[0]);
                parseParkplaetze(result[0]);
            }
            if (result[1] != null)
                parseParkplaetzeBelegung(result[1]);

        }
    }
}
