package de.htw.team06.db_parkplatz.Daten;

import org.json.JSONObject;

import de.htw.team06.db_parkplatz.R;

public class Parkplatz implements IParkplatz {

    public static final int DEFAULT_BELEGUNG_KATEGORIE_NUMMER = 0;
    public  static final String DEFAULT_BELEGUNG_KATEGORIE_TEXT = "n.A.";

    private static final String ID = "parkraumId";
    private static final String BEMERKUNG = "parkraumBemerkung";
    private static final String NAME = "parkraumDisplayName";
    private static final String OEFFNUNGSZEITEN = "parkraumOeffnungszeiten";
    private static final String STELLPLAETZE = "parkraumStellplaetze";
    private static final String PREIS_1_STUNDE = "tarif1Std";
    private static final String PREIS_1_TAG = "tarif1Tag";
    private static final String PREIS_1_MONAT = "tarif1MonatDauerparken";
    private static final String PARKDAUER_HINWEIS = "tarifParkdauer";
    private static final String LATITUDE = "parkraumGeoLatitude";
    private static final String LONGITUDE = "parkraumGeoLongitude";


    private int _ID = -1;
    private String _bemerkung = "";
    private String _name = "";
    private String _oeffnungszeiten = "";
    private int _stellplaetze = 0; //Insgesammt verfügbare
    private float _preis1Stunde = 0.0f;
    private float _preis1Tag = 0.0f;
    private float _preis1Monat = 0.0f;
    private String _parkdauerHinweis = "";
    private double _latitude = 0.0;
    private double _longitude = 0.0;
    private double _distance = 0.0;
    private int _belegungKategorieNummer = DEFAULT_BELEGUNG_KATEGORIE_NUMMER;
    private String _belegungKategorieText = DEFAULT_BELEGUNG_KATEGORIE_TEXT;

    public Parkplatz(JSONObject json) {
        _ID =               json.optInt(ID, -1);
        _bemerkung =        json.optString(BEMERKUNG, "");
        _name =             json.optString(NAME, "");
        _oeffnungszeiten =  json.optString(OEFFNUNGSZEITEN, "");
        _stellplaetze =     json.optInt(STELLPLAETZE, 0);
        _preis1Stunde =     getFloatFromJSONviaString(json, PREIS_1_STUNDE);
        _preis1Tag =        getFloatFromJSONviaString(json, PREIS_1_TAG);
        _preis1Monat =      getFloatFromJSONviaString(json, PREIS_1_MONAT);
        _parkdauerHinweis = json.optString(PARKDAUER_HINWEIS, "");
        _latitude  =        getFloatFromJSONviaString(json, LATITUDE);
        _longitude =        getFloatFromJSONviaString(json, LONGITUDE);
    }

    private float getFloatFromJSONviaString(JSONObject json, String key) {

        String val = json.optString(key, "0.0");
        float wert = 0.0f;
        try {
            wert = Float.valueOf(val.replace(',', '.'));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return wert;
    }

    @Override
    public String toString() {
        String buf = "{";

        buf += ID                   + ":" + _ID                     + ",";
        buf += BEMERKUNG            + ":" + _bemerkung              + ",";
        buf += NAME                 + ":" + _name                   + ",";
        buf += OEFFNUNGSZEITEN      + ":" + _oeffnungszeiten        + ",";
        buf += STELLPLAETZE         + ":" + _stellplaetze           + ",";
        buf += PREIS_1_STUNDE       + ":" + _preis1Stunde           + ",";
        buf += PREIS_1_TAG          + ":" + _preis1Tag              + ",";
        buf += PREIS_1_MONAT        + ":" + _preis1Monat            + ",";
        buf += PARKDAUER_HINWEIS    + ":" + _parkdauerHinweis       + ",";
        buf += LATITUDE             + ":" + _latitude               + ",";
        buf += LONGITUDE            + ":" + _longitude;

        return buf + "}";
    }

    @Override
    public int getID() {

        return _ID;
    }

    @Override
    public String getBemerkung() {

        return _bemerkung;
    }

    @Override
    public String getName() {

        return _name;
    }

    @Override
    public String getOeffnungszeiten() {

        return _oeffnungszeiten;
    }

    @Override
    public int getStellplaetze() {

        return _stellplaetze;
    }

    @Override
    public float getPreis1Stunde() {

        return _preis1Stunde;
    }

    @Override
    public float getPreis1Tag() {

        return _preis1Tag;
    }

    @Override
    public float getPreis1Monat() {

        return _preis1Monat;
    }


    @Override
    public double getLatitude() {

        return _latitude;
    }

    @Override
    public double getLongitude() {

        return _longitude;
    }



    @Override
    public int getBelegungKategorieNummer() {
        return _belegungKategorieNummer;
    }

    @Override
    public String getBelegungKategorieText() {
        return _belegungKategorieText;
    }

    @Override
    public void setBelegungKategory(int nummer, String text) {
        _belegungKategorieNummer = nummer;
        _belegungKategorieText = text;
    }

    public static int getMarkerPicIDperCategory(int kategorie) {
        switch (kategorie) {
            case(1): return R.drawable.marker_2_2_rot;
            case(2): return R.drawable.marker_2_2_orange;
            case(3): return R.drawable.marker_2_2_hellblau;
            case(4): return R.drawable.marker_2_2_gruen;
            default: return R.drawable.marker_2_2_grau;
        }
    }

    @Override
    public void setDistance(double meter) {

        this._distance = meter;
    }

    @Override
    public double getDistance() {

        return _distance; }
}
