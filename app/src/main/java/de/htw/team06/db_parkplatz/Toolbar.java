package de.htw.team06.db_parkplatz;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Toolbar extends Fragment {

    private ToolbarInterface activityOwner;

    public Toolbar() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //let it crash
        activityOwner = (ToolbarInterface) activity;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View t = inflater.inflate(R.layout.fragment_toolbar, container, false);
        t.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityOwner.onBackClick();
            }
        });
        return t;
    }


}


