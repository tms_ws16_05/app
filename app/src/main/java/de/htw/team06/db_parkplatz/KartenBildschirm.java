package de.htw.team06.db_parkplatz;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import de.htw.team06.db_parkplatz.Daten.DatenManager;
import de.htw.team06.db_parkplatz.Daten.IParkplatz;
import de.htw.team06.db_parkplatz.Daten.Parkplatz;


public class KartenBildschirm extends FragmentActivity implements OnMapReadyCallback, ToolbarInterface, PlaceSelectionListener {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private GoogleMap map;

    private Marker selectedMarker = null;

    private PlaceAutocompleteFragment such_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        such_bar = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.such_bar);
        such_bar.setOnPlaceSelectedListener(this);
        such_bar.setHint(getString(R.string.btn_txt_suchen));

    }

    @Override
    public void onMapReady(GoogleMap karte) {

        map = karte;

        //Info-Fenster für Marker
        map.setInfoWindowAdapter(getInfoWindowAdapter(map));

        DatenManager datenManager = DatenManager.getInstance();
        IParkplatz[] alleParkplaetze = datenManager.getAlleParkplaetze();

        LatLngBounds.Builder builder = new LatLngBounds.Builder(); //Bondary-(Grenzen) Objekt anlegen

        //Zeige Marker an, falls vorhanden
        int selectedParkplatzID = getIntent().getIntExtra("selectedParkplatz", -1);

        for (IParkplatz aktuellerParkplatz : alleParkplaetze) {

            //Der Parkplatz (ID=62) wird ignoriert, da Long-/Latitudeformat falsch (Ab Zeile 5829), diese werden sonst als 0 ausgewertet
            if (aktuellerParkplatz.getID() == 62) continue;

            LatLng currentLatLng = new LatLng(aktuellerParkplatz.getLatitude(), aktuellerParkplatz.getLongitude());

            builder.include(currentLatLng);

            Marker marker = map.addMarker(new MarkerOptions()
                    .position(currentLatLng)
                    .title(aktuellerParkplatz.getName())
                    .snippet(parkPlatzSnippetText(aktuellerParkplatz))
                    .icon(BitmapDescriptorFactory.fromResource(Parkplatz.getMarkerPicIDperCategory(aktuellerParkplatz.getBelegungKategorieNummer()))));

            //Bestimme ob gerade hinzugefügter Marker später Zoom erhalten soll
            if (selectedParkplatzID != -1) {
                if (aktuellerParkplatz.getID() == selectedParkplatzID)
                    selectedMarker = marker;
            }
        }

        //Boundaryobjekt füllen und auf Karte anwenden
        //Details http://stackoverflow.com/questions/14828217/android-map-v2-zoom-to-show-all-the-markers
        LatLngBounds bounds = builder.build();

        such_bar.setBoundsBias(bounds);

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10); // offset from edges of the map 12% of screen

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        map.animateCamera(cu);

        if (selectedMarker != null) {
            selectedMarker.showInfoWindow();

            zoomToLatLng(selectedMarker.getPosition());
        }
    }

    public GoogleMap.InfoWindowAdapter getInfoWindowAdapter(GoogleMap map) {
        return new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View infoWindow = getLayoutInflater().inflate(R.layout.marker_info_window, null);
                TextView parkplatzName = (TextView) infoWindow.findViewById(R.id.info_window_parkplatz_name);
                TextView parkplatzText = (TextView) infoWindow.findViewById(R.id.info_window_parkplatz_text);

                parkplatzName.setText(marker.getTitle());
                parkplatzText.setText(marker.getSnippet());

                return infoWindow;
            }
        };
    }
    /**
     * Gibt einen formatierten Beschreibungstext für den Marker des eingegebenen Parkplatzes zurück
     * @param aktuellerParkplatz das aktuelle Parkplatzobjekt
     * @return eine Zusammenfassung des Parkplatzes als String für das Markerinfofenster
     */
    private String parkPlatzSnippetText(IParkplatz aktuellerParkplatz) {

        String strStellplaetzeFormat = getResources().getString(R.string.info_text_stellplaetze);
        String strStellplaetzeMsg = String.format(strStellplaetzeFormat + "\n", aktuellerParkplatz.getStellplaetze());

        String strStellplaetzeFreiFormat = getResources().getString(R.string.info_text_stellplaetze_frei);
        String strStellplaetzeFreiMsg = String.format(strStellplaetzeFreiFormat + "\n", aktuellerParkplatz.getBelegungKategorieText());

        String strPreis1StundeFormat = getResources().getString(R.string.info_text_preis1stunde);
        String strPreis1StundeMsg = String.format(strPreis1StundeFormat + "\n", aktuellerParkplatz.getPreis1Stunde());

        String strPreis1TagFormat = getResources().getString(R.string.info_text_preis1tag);
        String strPreis1TagMsg = String.format(strPreis1TagFormat + "\n", aktuellerParkplatz.getPreis1Tag());

        String strPreis1MonatFormat = getResources().getString(R.string.info_text_preis1monat);
        String strPreis1MonatMsg = String.format(strPreis1MonatFormat + "\n", aktuellerParkplatz.getPreis1Monat());

        String strBemerkungenFormat = getResources().getString(R.string.info_text_bemerkungen);
        String strBemerkungenMsg = String.format(strBemerkungenFormat + "\n", aktuellerParkplatz.getBemerkung());

        String snippetText = "";

        snippetText += strStellplaetzeMsg;

        snippetText += strStellplaetzeFreiMsg;

        if (aktuellerParkplatz.getPreis1Stunde() != 0.0)
            snippetText += strPreis1StundeMsg;

        if (aktuellerParkplatz.getPreis1Tag() != 0.0)
            snippetText += strPreis1TagMsg;

        if (aktuellerParkplatz.getPreis1Monat() != 0.0)
            snippetText += strPreis1MonatMsg;

        if (aktuellerParkplatz.getOeffnungszeiten().equals(getResources().getString(R.string.durchgehend_geoeffnet))) {
            snippetText +=  getResources().getString(R.string.str_durchgehend_geoeffnet) + "\n";
        } else {
            snippetText += aktuellerParkplatz.getOeffnungszeiten() + "\n";
        }

        if (!aktuellerParkplatz.getBemerkung().isEmpty())
            snippetText += strBemerkungenMsg;

        return snippetText;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Map Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public void onBackClick() {
        finish();
    }


    private void zoomToLatLng(LatLng pos) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(pos)
                .zoom(12).build();
        //Zoom in and animate the camera.

        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


    @Override
    public void onPlaceSelected(Place place) {
        Log.d("suche", "Place Selected: " + place.getName());
        zoomToLatLng(place.getLatLng());
    }

    @Override
    public void onError(Status status) {
        Log.d("suche", "" + status.getStatus() + ": " + status.getStatusMessage());
    }
}
