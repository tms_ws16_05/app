package de.htw.team06.db_parkplatz;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.htw.team06.db_parkplatz.Daten.DatenManager;
import de.htw.team06.db_parkplatz.Daten.IParkplatz;
import de.htw.team06.db_parkplatz.Daten.ParkplatzListeAdapter;



public class InDerNaeheBildschirm extends FragmentActivity implements ToolbarInterface, GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;

    private Double latitude;
    private Double longitude;

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpGoogleApiClient();
        setContentView(R.layout.activity_listcurrentlocation);
    }

    private void setUpGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    /**
     * Initialisiert ListView und füllt dieses mit Daten
     * @param alleParkplaetze Liste mit allen Parkplatzobjekten
     */
    private void setUpListView(List<IParkplatz> alleParkplaetze) {
        ListView listView;
        ParkplatzListeAdapter adapter;

        listView = (ListView) findViewById(R.id.listView);
        adapter = new ParkplatzListeAdapter(this, alleParkplaetze);
        listView.setAdapter(adapter);
        adapter.notifyDataSetInvalidated();

        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3) {
                parent.getItemAtPosition(position);
                //Toast.makeText(getApplicationContext(),String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_LONG).show();
                Intent kartenBildschirm = new Intent(InDerNaeheBildschirm.this, KartenBildschirm.class);
                IParkplatz parkplatz =  (IParkplatz) parent.getItemAtPosition(position);
                kartenBildschirm.putExtra("selectedParkplatz", parkplatz.getID());
                startActivity(kartenBildschirm);
                }
        });
    }

    private List<IParkplatz> getIParkplatzArrayList() {
        DatenManager datenManager = DatenManager.getInstance();
        IParkplatz[] alleParkplaetze = datenManager.getAlleParkplaetze();
        alleParkplaetze = getDistances(alleParkplaetze);
        return getSortierteParkplatzListe(alleParkplaetze);
    }

    /**
     * Sortiere Parkplatzarray nach Abstand und gebe diese als Liste zurück
     * @param alleParkplaetze Array mit allen Parkplatzobjekten
     * @return Sortierte Parkplatzliste
     */
    private List<IParkplatz> getSortierteParkplatzListe(IParkplatz[] alleParkplaetze) {
        List<IParkplatz> parkPlatzListe = Arrays.asList(alleParkplaetze);
        Collections.sort(parkPlatzListe, new Comparator() {
            public int compare(Object o1, Object o2) {
                Double x1 = ((IParkplatz) o1).getDistance();
                Double x2 = ((IParkplatz) o2).getDistance();
                return x1.compareTo(x2);
            } }
        );
        return parkPlatzListe;
    }

    private IParkplatz[] getDistances(IParkplatz[] alleParkplaetze) {

        for (IParkplatz in : alleParkplaetze) {

            Location locationA = new Location("Current Location");

            locationA.setLatitude(latitude);
            locationA.setLongitude(longitude);

            Location locationB = new Location("Parkplatz Location");

            locationB.setLatitude(in.getLatitude());
            locationB.setLongitude(in.getLongitude());

            in.setDistance(locationA.distanceTo(locationB));

        }

        return alleParkplaetze;
    }


    @Override
    public void onBackClick() {
        finish();
    }

    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Berechtigung auf Standortdaten verweigert, prüfe ob der Nutzer dies bereits vormals abgelehnt hat
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_benoetigt_zugriff_auf_location), Toast.LENGTH_LONG).show();

            } else {
                //Benutzer wird zum ersten Mal aufgefordert die Berechtigungen zu erteilen
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        } else {
            // Berechtigung erteilt
            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            startLocationUpdates();
            if (mLastLocation != null) {
                latitude = mLastLocation.getLatitude();
                longitude = mLastLocation.getLongitude();
                setUpListView(getIParkplatzArrayList());
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_location_noch_nicht_empfangen), Toast.LENGTH_LONG).show();
            }
        }
    }
    public void onLocationChanged(Location location) {
        Toast.makeText(this, R.string.toast_neue_position_bestimmt, Toast.LENGTH_SHORT).show();
    }
   private void startLocationUpdates() {
        // Create the location request
       LocationRequest mLocationRequest = LocationRequest.create()
               .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
               .setInterval(1L)
               .setFastestInterval(1L);
        // Request location updates
       if (ContextCompat.checkSelfPermission(this,
               Manifest.permission.ACCESS_FINE_LOCATION)
               == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                        mLocationRequest, this);
       }

    }
    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // An unresolvable error has occurred and a connection to Google APIs
        // could not be established. Display an error message, or handle
        // the failure silently
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_location_unbekannter_fehler), Toast.LENGTH_LONG).show();
    }
}
