package de.htw.team06.db_parkplatz.Daten;

public interface IParkplatz {

    //parkraumId
    int getID();

    //parkraumBemerkung
    String getBemerkung();

    //parkraumDisplayName
    String getName();

    //parkraumOeffnungszeiten
    String getOeffnungszeiten();

    //parkraumStellplaetze
    int getStellplaetze();

    //tarif1Std
    float getPreis1Stunde();

    //tarif1Tag
    float getPreis1Tag();

    //tarif1MonatDauerparken
    float getPreis1Monat();

//    String getParkdauerHinweis();

    //parkraumGeoLatitude
    double getLatitude();

    //parkraumGeolongitude
    double getLongitude();

    //Abstand zu vorgegebenem Punkt
    void setDistance(double meter);

    //Abstand zu vorgegebenem Punkt
    double getDistance();



    // aktuelle Daten
    int getBelegungKategorieNummer();

    String getBelegungKategorieText();

    void setBelegungKategory(int nummer, String text);
}
