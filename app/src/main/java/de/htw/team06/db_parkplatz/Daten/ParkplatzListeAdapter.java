package de.htw.team06.db_parkplatz.Daten;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import de.htw.team06.db_parkplatz.R;


    public class ParkplatzListeAdapter extends ArrayAdapter<IParkplatz> {
        private static final int METER_IN_KILOMETER = 1000;

        public ParkplatzListeAdapter(Context context, List<IParkplatz> liste) {
            super(context,
                    R.layout.list_current_location_element,
                    liste);
        }


        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            Locale loc = Locale.getDefault();

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(
                        R.layout.list_current_location_element,
                        parent, false);
            }

            IParkplatz p = getItem(position);

            TextView tvParkplatzName = (TextView) convertView.findViewById(R.id.tv_parkplatz_name);
            tvParkplatzName.setText(p.getName());

            TextView tvParkplatzBelegung = (TextView) convertView.findViewById(R.id.tv_parkplatz_belegung);
            tvParkplatzBelegung.setText(p.getBelegungKategorieText());

            TextView tvParkplatzDistance = (TextView) convertView.findViewById(R.id.tv_parkplatz_distance);
            tvParkplatzDistance.setText(String.format(loc, "%.2f km", p.getDistance() / METER_IN_KILOMETER));

            return  convertView;
        }
    }