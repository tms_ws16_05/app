package de.htw.team06.db_parkplatz;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Toast;

import de.htw.team06.db_parkplatz.Daten.DatenManager;

public class HauptBildschirm extends Activity  {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__screen);

        DatenManager.init(this.getApplicationContext());

        askForPermissions();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }

    public void onSearchClick(View v) {
        Intent intent = new Intent(this, KartenBildschirm.class);
        startActivity(intent);
    }

    public void onNearMeClick(View v) {
        Intent intent = new Intent(this,
                InDerNaeheBildschirm.class);
        startActivity(intent);
    }

    private void askForPermissions() {
        // Berechtigungen, nach https://developer.android.com/training/permissions/requesting.html
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Berechtigung auf Standortdaten verweigert, prüfe ob der Nutzer dies bereits vormals abgelehnt hat
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(getApplicationContext(), R.string.toast_zugriffsfehler_location,
                    Toast.LENGTH_LONG).show();
            } else {
                //Benutzer wird zum ersten Mal aufgefordert die Berechtigungen zu erteilen
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        }
    }
}
