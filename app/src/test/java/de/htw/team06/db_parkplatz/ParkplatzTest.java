package de.htw.team06.db_parkplatz;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import de.htw.team06.db_parkplatz.Daten.Parkplatz;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
public class ParkplatzTest {

    private static final int TEST_ID = 4;
    private static final String TEST_NAME = "testBahnhof";
    private static final String TEST_PREIS_STUNDE_WITH_COMMA = "3,40";

    private static JSONObject _testParkplatzJSON;

    @BeforeClass
    public static void createTestParkplatz() {
        try {

            _testParkplatzJSON = new JSONObject();
            _testParkplatzJSON.put("parkraumId", TEST_ID);
            _testParkplatzJSON.put("parkraumDisplayName", TEST_NAME);
            _testParkplatzJSON.put("tarif1Std", TEST_PREIS_STUNDE_WITH_COMMA);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testIDandName() {
        Parkplatz p = new Parkplatz(_testParkplatzJSON);
        assertNotEquals("", p.getID());
        assertNotEquals("", p.getName());
    }

    @Test
    public void testDefaultFreiePlaetze() {
        Parkplatz p = new Parkplatz(_testParkplatzJSON);
        assertEquals(0, p.getBelegungKategorieNummer());
    }


    @Test
    public void testFloatWithCommas() {
        Parkplatz p = new Parkplatz(_testParkplatzJSON);
        assertNotEquals(0.0, p.getPreis1Stunde());
    }
}
