package de.htw.team06.db_parkplatz;

import android.content.Context;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import de.htw.team06.db_parkplatz.Daten.DatenManager;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class DatenManagerTest {

    @Mock
    private static Context c;

    @BeforeClass
    public static void initDatenManager() {
        DatenManager.init(c);
    }

    @Test
    public void checkDatenManagerNotNullAfterInit() {
        assertNotNull(DatenManager.getInstance());
    }




}
